/*
 * Copyright (C) 2021  Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#define POPULATION 10000           // size of the population
#define NUM_PRODUCT_TYPES 50       // number of the types of products in the economy
#define STEPS 1000                 // number of steps of the simulation to run
#define MAX_COST 10L               // maximum atomic cost of product (not including deps)
#define MIN_COST 0L
#define MEAN_COST 5L
#define STDDEV_COST 2L
#define MAX_NEED 1.0f
#define MIN_NEED 0.0f
#define MEAN_NEED 0.5f
#define STDDEV_NEED 0.2f
// TODO: Create separate environment variables for person need factor

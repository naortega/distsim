/*
 * Copyright (C) 2021  Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Person.hpp"

#include "Config.hpp"
#include "Random.hpp"

Person::Person() :
	balance(0)
{
	Random rand;
	for(unsigned int i = 0; i < NUM_PRODUCT_TYPES; ++i)
	{
		needMods.push_back(rand.genNormalRand(
					MEAN_NEED, STDDEV_NEED,
					MIN_NEED, MAX_NEED));
	}
}

/*
 * Copyright (C) 2021  Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Simulation.hpp"

#include <iostream>

#include "Config.hpp"

Simulation::Simulation()
{
	std::cout << "Initializing simulation..." << std::endl;
	std::cout << "Generating " << NUM_PRODUCT_TYPES <<
		" product types..." << std::endl;
	for(unsigned int i = 0; i < NUM_PRODUCT_TYPES; ++i)
	{
		productTypes.push_back(ProductType());
	}
	std::cout << "Generating " << POPULATION <<
		" persons..." << std::endl;
	for(unsigned int i = 0; i < POPULATION; ++i)
	{
		persons.push_back(Person());
	}
}

Simulation::~Simulation()
{
	std::cout << "Shutting down simulation..." << std::endl;
}

void Simulation::step()
{
}

void Simulation::printData() const
{
}
